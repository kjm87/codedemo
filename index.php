<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<style type="text/css">
  button 
  {
  	margin-left: 22px;
  }
  #footer
  {
    margin-top: 20px;
  }
  #error
  {
    color: red
  }
</style>
<title>Coding Demonstration</title>
  <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
  <meta name="ROBOTS" content="NOINDEX,NOFOLLOW,NOARCHIVE" />
  </head>
  <body><h1>Demo of Wordwrap Function</h1>
    <h2>Enter the text to be werapped</h2>
    <textarea id="textToWrap" rows="20" cols="180">
Replace this text with what you want to wrap
Lorem ipsum dolor sit amet, est et alterum epicurei vituperata, eirmod laoreet insolens ei his, mei eu illum minim constituto. Ut sint liberavisse vel, eam ad minim vivendo minimum, no ius audiam partiendo. Summo noluisse pertinax in his, porro tincidunt mea eu. Nam at autem sonet denique Te odio idque est, eu erant indoctum eam. Augue volutpat sea no, ei vix salutatus vulputate. Choro saperet pertinacia et ius, in has dicat gubergren constituto. Id per movet luptatum reprehendunt, pro adhuc mentitum ex. Solet aliquando ad quo, suas ludus inermis ex sed. Mutat deleniti inimicus in eos. Qui maiorum tractatos ne, vel ad dicat doctus voluptatum. Usu et tale pertinacia reformidans, et laoreet iracundia vix, usu inermis blandit sapientem a No vix maiorum invidunt tractatos. Pri inani partiendo in. Duo scripserit necessitatibus in, at quaeque imperdiet definitiones nec, his alia definiebas efficiendi ne. Ea ridens lucilius scribentur pro, ut dolore quidam nec. Ei intellegat intellegebat his. Mei nominavi recusabo ei, nonumy diceret docendi te qui. Soleat senserit mediocritatem eum cu, usu integre euripidis cu, ut vim clita scripta convenire. Qui veniam gloriatur cotidieque no, postulant vulputate sed eu. Vel eu vero aliquam perpetua. Hinc luptatum percipitur et ius, diam officiis eu duo. Ex labores phaedrum vis, no per dolor vocent Mentitum repudiare mediocritatem sea id, ut has veniam euripidis, disputationi conclusionemque pri ex. Dolorum hendrerit intellegat ad nam. Affert libris pri eu. Nec et assum novum aeterno, omnis erant usu ut, ius habeo corpora ad. Ei hinc natum duo, per et tempor singulis, vix et fugit facer voluptua. Mutat libris eripuit ius eu, cu usu facete percipit. Ne per mutat persius consectetuer. Sit atqui alienum eu, inani solet gloriatur at ius. In qui affert sanctus, vix ne nemore sensibus argumentum, eam ceteros consequat forensibus in. Reque virtute atomorum id vim, eu tale doming laoreet has. Quod harum oporteat eu qui, solet imperdiet instructior qui id. Sea unum virtute ei. An vel hinc nostro phaedrum, ex per cetero salutatus. Sumo dignissim mnesarchum cu per, vel ex minimum ancillae, latine quaestio dignissim mea in. Noluisse scribentur eloquentiam cum te. Te sale expetenda sed, elit autem aliquam an quo, magna congue vel eu. Eu vis mundi laudem, graece luptatum efficiantur vel ad, alienum mediocrem erroribus sea cu. Delectus recusabo et nec, agam eirmod utamur est id. Ei error ubique his, est vero erant ea, sumo nostrum singulis et nam. Postea imperdiet aliquando vis ne, vim labitur recteque no, erant tamquam ea mea. Ex mea iriure eleifend, ad sed laudem efficiantur. Sed quidam detracto ex, mea nemore option honestatis ad, oporteat singulis evertitur te est. Qui ex nisl tale suscipiantur, laoreet laboramus adolescens cum eu. Usu an stet unum, at eripuit maluisset nam. At duo melius dissentias, mel ullum commodo voluptaria cu.
</textarea>
    <p>Length of wrap line:<input id="wrapLength"/><button id="wrapNow">Wrap now</button></p>
    <h2>Wrapped Result</h2>
    <div id="wrappedResult"></div>
    <p id="footer">For access to the source code see the <a href="https://bitbucket.org/kjm87/codedemo/overview" target="_blank">bitbucket repository</a>.</p>
	<script>
	$( "#wrapNow" ).click(function() {
      // Get the values from elements on the page:
      var textTowrap = $( "#textToWrap" ).val(),
      lengthToWrap = $( "#wrapLength" ).val();
      // Send the data using post
      var posting = $.post( "wrap.php", { textTowrap: textTowrap, lengthToWrap: lengthToWrap } );
 
      // Put the results in a div
      posting.done(function( data ) {
        $( "#wrappedResult" ).empty().append( data );
      });
    });
</script>
  </body>
</html>