# Demo for showing coding skills
This project is to demonstrate key programming skills as requestyed by a recruiter

The initial breif was:

The task is to write a function called 'wrap' that takes two arguments, $string and $length.

The function should return the string, but with new lines ("\n") added to ensure that no line is longer than $length characters. Always wrap at word boundaries if possible, only break a word if it is longer than $length characters. When breaking at word boundaries, replace all the whitespace between the two words with a single newline character. Any unbroken whitespace should be left unchanged.

Please implement the function directly in PHP, rather than using the built-in wordwrap() function.

If you have any questions about the description above, please contact hiring manager for clarification.

.
.
.

Clarification questions asked:

1) Should the maximum line length in the returned string result include the \n character?

2) What should the function do/return if the $length argument is not a number or less than 1 (or 2 if Q1 is affirmative)? Classic behaviour would be to return FALSE. An alternative would be to throw an InvalidArgumentException. 

3) What should the function do/return when $string argument is empty or not a string?

4) If the $string argument starts with (or ends with) whitespace, should the function return a string that starts with (or ends with) \n (where the wrapping would break within that whitespace).

5) Is the definition of "word boundary" the same as the \b escape sequence in Perl-Compatible Regular Expressions.

Answeres:

1) No, only space with visual impact should count towards length. Don't worry about tabs and the like, count them as a single character.

2) An exception should be thrown if the length is out of bound or NaN.

3) Empty strings are easy to wrap, and if its not a string then an exception should be thrown.

4) Whitespace should be maintained, except where wrapping a string at a space in which case it should be converted to "\n"

5) Yes, \b as processed by PCRE covers our definition.
