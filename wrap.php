<?php  
error_reporting(E_ALL);
ini_set('html_errors', true);
# Author: Keith Morris; 
# Date 30 August 2011

function wrap($string, $length)
{
  # Check for valid input
  if ( !is_string ($string))
  {
    throw new InvalidArgumentException("The value for wrap text is not a vlaid string.");
  }
  
  #Option for length validation
  $options = array(
      'options' => array(
          'min_range' => 1
      ),
  );
  if ( filter_var($length, FILTER_VALIDATE_INT, $options) === FALSE)
  {
    throw new InvalidArgumentException("The value for wrap length is not vlaid.");
  }

  # Check for empty string
  if ($string == "") 
  {
    return "";
  }
  // Do not try to split if string length is already less than wrap length
  if (mb_strlen($string) <= $length)
  {
    return $string;
  }
 
  # $patternQuirk is required because "{$length}" == "70" when $length == 70 
  $patternQuirk = "{" . $length . "}";
  
  
  # match - up to $length stopping at 
  #      a word broundry or  
  #      whitespace up to a word boundry or 
  #      exactly the length
  # This splliting tequniqe relies on the regular expression engine being "greedy" 
  # See section on greedy here: http://php.net/manual/en/regexp.reference.repetition.php
  $matchPattern = "/^((.{1,$length}(?<=\\S)\\b)|(\\s{1,$length}\\b)|(.$patternQuirk))/";
  
  // A special overide of an edge case
  if ($length == 1)
  {
    $matchPattern = "/./";
  }
  
  $strings = array();
  # Add the patter to the output when debugging is required 
  # $strings[] = $matchPattern;
  
  $loopBreakMax = mb_strlen($string) + 1;
  $loopBreak = 0;
  $processedString = $string;
 
  # build array of wrapped lines
  while (preg_match($matchPattern, $processedString, $matches) )
  {
    // Capture the wrapped portion
    $strings[] = $matches[0];
    
    // Shorten the string to be processed by the number of characters already matched
    $processedString = mb_substr($processedString, mb_strlen($matches[0]));
        
    // If the remaining string is not whitespage trim the whitespce from the start of the string
    if (ltrim($processedString) != "")  
    {
    	$processedString = ltrim($processedString);
    }
    $loopBreak++;
    if ($loopBreak > $loopBreakMax)
    {
      throw new Exception("The function is failing to wrap the string.");
    }
  }
  # If there are some, add the last remaining characters to the string
  if ($processedString != "")
  {
  	$strings[] = $processedString;
  }
  
  # return joined lines
  return implode("\n", $strings);
}

function ajaxResponse()
{
  #check that this was a post request
  $result = "";
  if (isset($_POST['textTowrap']) and isset($_POST['lengthToWrap']))
  {
    try {#run the function
      error_log("Input: '$_POST[textTowrap]' ($_POST[lengthToWrap])");
      $wrappedText = wrap($_POST['textTowrap'], $_POST['lengthToWrap']);
      error_log("Result: '$wrappedText'");
      $result = "<pre>$wrappedText</pre>";
    } catch (Exception $E)
    { #catch any errors
	    $result = "<p id='error'>There was an error. <br/>" . $E->getMessage() . "</p>";  
    }
  }
  else
  {
    $result = "<p id='error'>No data was supplied to wrap.</p>";
  }
  #return the results
  return $result;
}
  
echo ajaxResponse();